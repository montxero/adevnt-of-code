

(in-package #:advent)


(defun get-1a-data ()
  (let ((eof (gensym)))
    (with-open-file (s (get-data-path "1a"))
      (do ((xs ())
           (ys ())
           (f t (not f))
           (v (read s nil eof nil)  (read s nil eof nil)))
          ((eq eof v) (list xs ys))
        (if f (push v xs) (push v ys))))))


(defun 1a ()
  (destructuring-bind (xs ys) (mapcar (lambda (x) (sort x #'<)) (get-1a-data))
    (apply #'+ (mapcar (lambda (a b) (if (> a b) (- a b) (- b a))) xs ys))))

 (format t "~A~%" (1a))


(defun 1b ()
  (let ((ytable (make-hash-table :size 500))
        (seen-table (make-hash-table :size 500))
        (total 0))
    (destructuring-bind (xs ys) (get-1a-data)
      (dolist (y ys)
        (incf (gethash y ytable 0)))
      (dolist (x xs total)
        (unless (gethash x seen-table)
          (incf total (* x (gethash x ytable 0))))))))

 (format t "~A~%" (1b))
