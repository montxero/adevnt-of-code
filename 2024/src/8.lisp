(in-package :advent)


(defun 8-data (fname)
  (let ((grid (char-grid<-file fname))
        (table (make-hash-table :size 50)))
    (destructuring-bind (rows cols) (array-dimensions grid)
      (dotimes (i (* rows cols) (list table grid))
        (let ((c (row-major-aref grid i)))
          (unless (char= #\. c)
            (push (row-col<-row-major-index i rows) (gethash c table ()))))))))


(defun 8a (data)
  (destructuring-bind (table grid) data
    (let ((antinodes (make-hash-table :test 'equal :size (array-total-size grid))))
      (loop for vs being the hash-values of table
            do (let ((combos (combinations vs 2)))
                 (loop for xs = (funcall combos) then (funcall combos)
                       while xs
                       do (destructuring-bind (p1 p2) xs
                            (let* ((dp (list- p1 p2))
                                   (pa (list+ p1 dp))
                                   (pb (list+ pa dp))
                                   (pc (list- p1 dp))
                                   (pd (list- pc dp)))
                              (cond ((equal pa p2)
                                     (dolist (px (list pb pc))
                                       (when (point-in-grid-p grid px)
                                         (incf (gethash px antinodes 0)))))
                                    (t (dolist (px (list pa pd))
                                         (when (point-in-grid-p grid px)
                                           (incf (gethash px antinodes 0)))))))))))
      (hash-table-count antinodes))))



(defun 8b (data)
  (destructuring-bind (table grid) data
    (let ((antinodes (make-hash-table :test 'equal :size (array-total-size grid))))
      (loop for vs being the hash-values of table
            do (let ((combos (combinations vs 2)))
                 (loop for xs = (funcall combos) then (funcall combos)
                       while xs
                       do (destructuring-bind (p1 p2) xs
                             (incf (gethash p1 antinodes 0))
                            (let ((dp (list- p1 p2)))
                              (loop for px = (list+ p1 dp) then (list+ px dp)
                                    while (point-in-grid-p grid px)
                                    do (incf (gethash px antinodes 0)))
                              (loop for px = (list- p1 dp) then (list- px dp)
                                    while (point-in-grid-p grid px)
                                    do (incf (gethash px antinodes 0))))))))
      (hash-table-count antinodes))))



