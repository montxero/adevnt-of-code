#||
Utilities for AOC 2024
||#

(defpackage #:advent
  (:use #:cl)
  (:export :map-file)
  (:import-from :xero.utils partition))

(in-package #:advent)



(defparameter +data-directory+
  (make-pathname :directory "home/xero/common-lisp/advent/2024/data")
  "Data directory")


(defvar *print-debug* nil "Flag for debugging")


(defun debug-print (format-string &rest args)
  "Print function for debugging."
  (when *print-debug*
    (apply #'format t format-string args)))

(defun map-stream (function stream)
  "Apply FUNCTION to every Lisp expression in STREAM and return NIL."
  (let ((eos (gensym)))
    (do ((x (read stream nil eos nil) (read stream nil eos nil)))
        ((eq x eos) nil)
      (funcall function x))))


(defun map-file (function filespec)
  "Apply FUNCTION to every Lisp expression in the file associated with FILESPEC and return NIL."
  (with-open-file (s filespec)
    (map-stream function s)))


(defun lines<-file (filespec)
  "Return a list of the lines contiained in the file described by FILESPEC"
  (with-open-file (s filespec)
    (let ((eof (gensym)))
      (do ((lines () (push line lines))
           (line (read-line s nil eof nil) (read-line s nil eof nil)))
          ((eq line eof) (nreverse lines))))))


(defun get-data-path (question)
  "Return the PATH object pointing to the file for QUESTION

Parameter
---------
QUESTION: STRING, the question to be solved.

Returns
-------
Path object

Example
-------

(get-data-path \"1a\")
  #P\"/home/xero/common-lisp/advent/data/1a\"
"
  (merge-pathnames +data-directory+ question))


(defun grid<-list (rows cols type lst)
  "Return an array with dimensions ROWS x COLS where each index holds the corresponding element in lst.
All elements in the grid are of type TYPE."
  (do ((grid (make-array (list rows cols) :element-type type))
       (xs lst (cdr xs))
       (row (car lst) (car xs))
       (r 0 (1+ r)))
      ((or (null xs)) grid)
    (dotimes (c (length (car xs)))
      (setf (aref grid r c) (elt (car xs) c)))))


(defun char-grid<-file (datapath)
  (let ((eof (gensym)))
    (with-open-file (s (get-data-path datapath))
      (do ((lst () (push line lst))
           (line (read-line s nil eof nil) (read-line s nil eof nil))
           (rows 0 (1+ rows))
           (cols 0 (max cols (length line))))
          ((eq eof line)
           (do ((grid (make-array (list rows cols) :element-type 'character))
                (r 0 (1+ r))
                (xs (nreverse lst) (cdr xs)))
               ((null xs) grid)
             (let ((str (car xs)))
               (dotimes (c (length str))
                 (setf (aref grid r c) (char str c))))))))))

(defun row-col<-row-major-index (index nrows)
  "Return the pair (ROW COL) of the indices that corresponds to the row major index INDEX."
  (multiple-value-bind (r c) (floor index nrows)
    (list r c)))


;; Combinations and Permutations gotten from https://lists.gnu.org/archive/html/help-gnu-emacs/2009-06/msg00094.html
(defun permutations (bag)
   "Return a list of all the permutations of the input."
   ;; If the input is nil, there is only one permutation:
   ;; nil itself
   (if (null bag)
       (list ())
       ;; Otherwise, take an element, e, out of the bag.
       ;; Generate all permutations of the remaining elements,
       ;; And add e to the front of each of these.
       ;; Do this for all possible e to generate all permutations.
       (mapcan #'(lambda (e)
                   (mapcar #'(lambda (p) (cons e p))
                           (permutations (remove e bag :count 1))))
               bag)))


(defun combinations (sequence n)
  "Return a closure that holds all the N-combinations of elements of SEQUENCE.
When the combinations are complete, calling the closure returns NIL"
  (let ((table (make-hash-table))
        (i 0))
    (xero.utils:dosequence (x sequence)
      (setf (gethash i table) x)
      (incf i))
    (let ((fmt-string (format nil "~~~A,'0,B" (hash-table-count table)))
          (i (expt 2 (hash-table-count table))))
      (labels ((next ()
                 (decf i)
                 (if (= n (logcount i)) i (next))))
        (lambda ()
          (next)
          (unless (minusp i)
            (let ((str (format nil fmt-string i)))
              (loop for j below (length str)
                    when (char= #\1 (char str j))
                      collect (gethash j table)))))))))

(defun point-in-grid-p (grid point)
  "Return T iff point is in the grid"
  (apply #'array-in-bounds-p grid point))



(defun gref (grid position)
  "Return the element at position in grid"
  (apply #'aref grid position))


(defun grid-adjacents (grid position)
  "return a list of all the indicies adjacent (top bottom left right) to position"
  (remove-if-not (lambda (p) (point-in-grid-p grid p))
                 (mapcar (lambda (delta) (list+ position delta))
                         (list (list -1 0) (list 1 0) (list 0 -1) (list 0 1)))))


(defun list+ (xs ys)
  "Return the list resulting from adding XS and YS element wise"
  (mapcar #'+ xs ys))

(defun list- (xs ys)
  "Return the list resulting from subtracting Ys from XS element wise"
  (mapcar #'- xs ys))

(defun abs-list- (xs ys)
  "Return the list resulting from taking the absolute values of the elment wise difference between XS and YS."
  (mapcar #'abs (list- xs ys)))

(defun igroup (source n)
  (let ((src (copy-seq source)))
    (flet ((emptyp (xs)
             (cond ((listp xs) (null xs))
                   ((arrayp xs) (zerop (length xs)))) ))
      (lambda ()
        (unless (emptyp src)
          (prog1 (xero.utils:take n src)
            (setf src (xero.utils:drop n src))))))))
