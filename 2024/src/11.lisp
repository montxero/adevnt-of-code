(in-package :advent)

(defun 11-split (seq n)
  (list (parse-integer (subseq seq 0 n))
        (parse-integer (subseq seq n))))


(defun 11-data (fname)
  (with-open-file (s fname)
          (loop for eof = (gensym) then eof
                for d = (read s nil eof nil) then  (read s nil eof nil)
                until (eq d eof)
                collect d)))



(let ((table (make-hash-table :test 'equal :size 1000000000)))
  (defun 11-pass (n blinks)
    (labels ((rec (i b)
               (if (zerop i)
                   (11-pass 1 (1- b))
                   (let ((s (write-to-string n)))
                     (if (oddp (length s))
                         (11-pass (* i 2024) (1- b))
                         (apply #'+ (mapcar (lambda (a) (11-pass a (1- b))) (11-split s (/ (length s) 2)))))))))
      (if (zerop blinks)
          1
          (let ((d (list n blinks)))
            (unless (gethash d table)
              (setf (gethash d table) (rec n blinks)))
            (gethash d table)))))

  (defun 11-table () table))

(defun 11-sol (data blinks)
  (reduce #'(lambda (a b) (+ a (11-pass b blinks))) data :initial-value 0 ))

(defun 11a (data) (11-sol data 25))
(defun 11b (data) (11-sol data 75))
