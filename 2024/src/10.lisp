(in-package :advent)

(defun 10-data (fname)
  (with-open-file (s fname)
    (loop for eof = (gensym) then eof
          for line = (read-line s nil eof nil) then (read-line s nil eof nil)
          for rows = 0 then (1+ rows)
          for cols = (length line) then cols
          until (eq eof line)
          collect line into lines
          finally (return
                    (loop for v = (make-array (list rows cols) :element-type 'integer) then v
                          for xs = lines then (cdr xs)
                          for r below rows
                          until (null xs)
                          do (loop for j below cols
                                   do (setf (aref v r j) (digit-char-p (char (car xs) j))))
                          finally (return v))))))


(10-data "/home/xero/common-lisp/advent/2024/data/10t")


(defun 10a-search (array index)
  (let ((paths ())
        (visited (make-hash-table :test #'equal :size (array-total-size array))))
    (labels ((rec (i current-path)
               (setf (gethash i visited) t)
               (let ((p (gref array i)))
                 (cond ((= 9 p)
                        (push (cons i current-path) paths))
                       (t
                        (let ((new-path (cons i current-path)))
                          (dolist (c (candidates i p))
                            (rec c new-path)))))))
             (candidates (i p)
               (remove-if (lambda (n)
                            (or (/= (gref array n) (1+ p))
                                (gethash n visited)))
                          (grid-adjacents array i))))
      (rec index ())
      (length paths))))

(10a-search (10-data "/home/xero/common-lisp/advent/2024/data/10t") (list 0 0))

(defun 10a (data)
  (let ((count 0)
        (nrows (car (array-dimensions data))))
    (dotimes (i (array-total-size data) count)
      (when (zerop (row-major-aref data i))
        (incf count (10a-search data (row-col<-row-major-index i nrows)))))))


(defun 10b-search (array index)
  (let ((paths ()))
    (labels ((rec (i current-path)
               (let ((p (gref array i)))
                 (cond ((= 9 p)
                        (push (cons i current-path) paths))
                       (t
                        (let ((new-path (cons i current-path)))
                          (dolist (c (candidates i p))
                            (rec c new-path)))))))
             (candidates (i p)
               (remove-if (lambda (n)
                            (/= (gref array n) (1+ p)))
                          (grid-adjacents array i))))
      (rec index ())
      (length paths))))

(defun 10b (data)
  (let ((count 0)
        (nrows (car (array-dimensions data))))
    (dotimes (i (array-total-size data) count)
      (when (zerop (row-major-aref data i))
        (incf count (10b-search data (row-col<-row-major-index i nrows)))))))
