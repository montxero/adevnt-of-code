
(in-package :advent)



(defun 12a (data)
  (let ((seen (make-hash-table :test 'equal :size (* 2 (array-total-size data)))))
    (labels ((seenp (position)
               (gethash position seen))
             
             (area (region)
               (length region))
             
             (perimeter (region)
               (- (* 4 (length region))
                  (reduce (lambda (a b) (+ a (length (adjacents b)))) region :initial-value 0)))

             (adjacents (position)
               (let ((p (pref position)))
                 (remove-if (lambda (x) (char/= p (pref x))) (grid-adjacents data position))))

             (pref (position)
               (gref data position))

             (get-region (to-visit acc)
               (if (null to-visit) (remove-duplicates acc :test #'equal)
                   (destructuring-bind (hd . tl) to-visit
                     (setf (gethash hd seen) t)
                     (let ((ps (remove-if (lambda (p) (seenp p)) (adjacents hd))))
                       (get-region (append ps tl) (cons hd acc)))))))
      
      (loop for i below (array-total-size data)
            for rs = (car (array-dimensions data)) then rs
            for pos =  (row-col<-row-major-index i rs)
            for price = 0 then price
            do (unless (seenp pos)
                 (let ((region (get-region (list pos) ())))
                   (format t "~%~A: perimeter ~A, area ~A region= ~A~%" (pref pos) (perimeter region) (area region) region)
                   (incf price (* (area region) (perimeter region)))))
            finally (return price)))))


(12a (char-grid<-file (get-data-path "12")))
