(in-package :advent)


(defun 4-grid (data-path)
  (let ((eof (gensym)))
    (with-open-file (s (get-data-path data-path))
      (do ((lst () (push line lst))
           (line (read-line s nil eof nil) (read-line s nil eof nil))
           (rows 0 (1+ rows))
           (cols 0 (max cols (length line))))
          ((eq eof line)
           (do ((grid (make-array (list rows cols) :element-type 'character))
                (r 0 (1+ r))
                (k 0 (incf k))
                (xs (nreverse lst) (cdr xs)))               
               ((null xs) grid)
             (let ((str (car xs)))
               (dotimes (c (length str))
                 (setf (aref grid r c) (char str c))))))))))


(defun directional-search (x y dx dy target grid)
  "Search for TARGET in GRID starting from position X,Y and moving in increments of DX and DY.
If TARGET is found at position X Y, return a list of the positions of the letters of TARGET

Parameters
----------
POSITION: (fixum fixnum)
DX: fixnum
DY: fixnum
TARGET: string
GRID: 2d Array of characters

Returns
-------
List of points
"
  (let ((xlim (+ x (* dx (1- (length target)))))
        (ylim (+ y (* dy (1- (length target))))))
    (debug-print "xlim:~A ylim:~A dim: ~A~%" xlim ylim (array-dimensions grid))
    (when (array-in-bounds-p grid xlim ylim)
      (debug-print "in bounds!")
      (do ((i x (+ i dx))
           (j y (+ j dy))
           (k 0 (1+ k))
           (acc () (push (list i j) acc))
           (samep t (char= (aref target k) (aref grid i j))))
          ((or (not samep) (= k (length target)))
           (and samep  (nreverse acc)))))))


(defun search-forward-horizontal (x y target grid)
  (directional-search x y 0 1 target grid))

(defun search-backward-horizontal (x y target grid)
  (directional-search x y 0 -1 target grid))

(defun search-down-vertical (x y target grid)
  (directional-search x y 1 0 target grid))

(defun search-up-vertical (x y target grid)
  (directional-search x y -1 0 target grid))

(defun search-l-r-up-diagonal (x y target grid)
  (directional-search x y -1 1 target grid))

(defun search-l-r-down-diagonal (x y target grid)
  (directional-search x y 1 1 target grid))

(defun search-r-l-up-diagonal (x y target grid)
  (directional-search x y -1 -1 target grid))

(defun search-r-l-down-diagonal (x y target grid)
  (directional-search x y 1 -1 target grid))

(defun search-for-target (x y target grid)
  (remove-if #'null
   (mapcar (lambda (f) (funcall f x y target grid))
           (list #'search-forward-horizontal
                 #'search-backward-horizontal
                 #'search-up-vertical
                 #'search-down-vertical
                 #'search-l-r-up-diagonal
                 #'search-l-r-down-diagonal
                 #'search-r-l-up-diagonal
                 #'search-r-l-down-diagonal))))


(defun 4a (grid)
  (let ((result 0))
    (destructuring-bind (rows cols) (array-dimensions grid)
      (dotimes (r rows result)
        (dotimes (c cols)
          (incf result (length (search-for-target r c "XMAS" grid)))
          (let ((xx  (search-for-target r c "XMAS" grid)))
            (if xx
                (debug-print "~&~A~%" xx)
                (debug-print "."))))))))


(defun diagonal-search (x y target grid)
  (remove-if #'null
   (mapcar (lambda (f) (funcall f x y target grid))
           (list #'search-l-r-up-diagonal
                 #'search-l-r-down-diagonal
                 #'search-r-l-up-diagonal
                 #'search-r-l-down-diagonal))))



(defun 4b (grid)
  (let ((table (make-hash-table :test 'equal))
        (result 0))
    (destructuring-bind (rows cols) (array-dimensions grid)
      (dotimes (r rows)
        (dotimes (c cols)
          (dolist (match (diagonal-search r c "MAS" grid))
            (incf (gethash (cadr match) table 0))))))
    (maphash (lambda (key val)
               (declare (ignore key))
               (if (= 2 val) (incf result)))
             table)
    result))
