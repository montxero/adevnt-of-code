(in-package :advent)


(defun 7-data (fname)
  (let ((table (make-hash-table :size 200))
              (eof (gensym)))
          (with-open-file (s (get-data-path fname))
            (do ((line (read-line s nil eof nil) (read-line s nil eof nil)))
                ((eq line eof) table)
              (destructuring-bind (target eqn) (cl-ppcre:split ":" line)
                (push (mapcar #'parse-integer (cl-ppcre:split " " (string-trim (list #\SPACE) eqn))) (gethash (parse-integer target) table)))))))



;; One way to tackle the problem is as follows
;; For each equation,
;; i. explicitly generate every possible expression then,
;; ii. evaluate each expression in turn and compare it with the target
;;
;; The approach taken here is as follows,
;; For each equation,
;; Implicitly evaluate each expression in turn and compare the result with the target.
;;
;; The implict evaluation is done by exploiting the following observation.
;; i. There are only two types of operators `+` and `*`.
;; ii. The number of operators is always one less than the length of the operands of the equation.
;; iii. The binary representation of the numbers (0, 1, ..., length of operands - 1) can be used to
;;      represent every possible combination of operators for the equations.
;; iv. Equations are evaluated left to right without any attention to order of operations.



(defun 7a-evaluate-equation (i op-str acc operand-list)
  (if (null operand-list)
      acc
      (let ((op (char op-str i))
            (x (car operand-list))
            (xs (cdr operand-list)))
        (if (char= op #\0)
            (7a-evaluate-equation (1+ i) op-str (+ acc x) xs)
            (7a-evaluate-equation (1+ i) op-str (* acc x) xs)))))


(defun 7a-evaluate (op-str operand-list)
  (7a-evaluate-equation 0 op-str (car operand-list) (cdr operand-list)))


;; (defun 7-op-str<-n (n length)
;;   (let ((st (format nil "~~~A,'0,B" length)))
;;     (format nil st n)))


(defun 7-op-str<-n (n length base)
  (let ((st (format nil "~~~A,~A,'0,R" base length)))
    (format nil st n)))


(defun 7a-generate-and-evaluate (operand-list)
  (let ((res ())
        (len (1- (length operand-list))))
    (dotimes (i (expt 2 (1+ len)) res)
      (push (7a-evaluate (7-op-str<-n i len 2) operand-list) res))))


(defun 7a (fname)
  (let ((table (7-data fname))
        (result 0))
    (maphash (lambda (target eqns)
               (dolist (eqn eqns)
                 (let ((candidates (7a-generate-and-evaluate eqn)))
                   (when (member target candidates)
                     (incf result target)))))
             table)
    result))


(defun 7-evaluate-equation (i op-str acc operand-list)
  (if (null operand-list)
      acc
      (let ((op (char op-str i))
            (x (car operand-list))
            (xs (cdr operand-list)))
        (cond ((char= op #\0)
               (7-evaluate-equation (1+ i) op-str (+ acc x) xs))
              ((char= op #\1)
               (7-evaluate-equation (1+ i) op-str (* acc x) xs))
              ((char= op #\2)
               (7-evaluate-equation (1+ i)
                                    op-str
                                    (parse-integer (format nil "~A~A" acc x))
                                     xs))))))






(defun 7b (fname)
  (let ((table (7-data fname))
        (result 0))
    (maphash (lambda (target eqns)
               (dolist (eqn eqns)
                 (let ((candidates (7b-generate-and-evaluate eqn)))
                   (when (member target candidates)
                     (incf result target)))))
             table)
    result))


(defun 7b-generate-and-evaluate (operand-list)
  (let ((res ())
        (len (1- (length operand-list))))
    (dotimes (i (expt 3 (1+ len)) res)
      (push (7b-evaluate (7-op-str<-n i len 3) operand-list) res))))

(defun 7b-evaluate (op-str operand-list)
  (7-evaluate-equation 0 op-str (car operand-list) (cdr operand-list)))
