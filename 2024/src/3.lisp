(in-package :advent)


(defun 3a ()
  (let ((eof (gensym)))
    (with-open-file (s (get-data-path "3"))
      (do ((line (read-line s nil eof nil) (read-line s nil eof nil))
           (result 0))
          ((eq eof line) result)
        (cl-ppcre:do-matches-as-strings (str "mul\\(\\d{1,3},\\d{1,3}\\)" line)
          (incf result (apply #'* (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings "\\d+" str)))))))))



(defun 3b ()
  (let ((eof (gensym)))
    (with-open-file (s (get-data-path "3"))
      (do ((line (read-line s nil eof nil) (read-line s nil eof nil))
           (result 0)
           (mulp t))
          ((eq eof line) result)
        (cl-ppcre:do-matches-as-strings (str "mul\\(\\d{1,3},\\d{1,3}\\)|don't|do" line)
          (cond ((string= "do" str) (setf mulp t))
                ((string= "don't" str) (setf mulp nil))
                (t
                 (when mulp
                   (incf result
                         (apply #'* (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings "\\d+" str))))))))))))
