(in-package :advent)


(defun 5-data (data-path)
  (let ((eof (gensym)))
    (with-open-file (s data-path)
      (do ((table (make-hash-table :size 300))
           (updates ())
           (line (read-line s nil eof nil) (read-line s nil eof nil)))
          ((eq eof line) (list table updates))
        (cond ((some (lambda (c) (char= c #\|)) line)
               (destructuring-bind (a b) (mapcar #'parse-integer (cl-ppcre:split "\\|" line))
                 (push b (gethash a table ()))))
              (t (let ((xs (mapcar #'parse-integer (cl-ppcre:split "," line))))
                   (when xs
                     (push (coerce xs 'vector) updates)))))))))


(defun update-ordered-p (update order-dict)
  (do* ((i (1- (length update)) (1- i))
       (ordered? t)
       (x (aref update (1- (length update))) (aref update i)))
      ((or (zerop i) (null ordered?))  ordered?)
    (let ((ys (gethash x order-dict)))
      (dotimes (j i)
        (when (member (aref update j) ys)
          (setf ordered? nil)
          (return))))))


(defun aref-mid (xs)
  (aref xs (floor (length xs) 2)))


(defun 5a (data)
  (let ((updates (cadr data))
        (order-dict (car data)))
    (apply #'+ (mapcar (lambda (xs) (or (and (update-ordered-p xs order-dict) (aref-mid xs)) 0)) updates))))





(defun fix-order (xs dict)
  (labels ((rec (i j)
             (cond ((zerop i) xs)
                   ((= j i) (rec (1- i) 0))
                   (t (let ((x (aref xs i))
                            (y (aref xs j)))
                        (cond ((member y (gethash x dict))
                               (setf (aref xs i) y)
                               (setf (aref xs j) x)
                               (rec i j))
                              (t (rec i (1+ j)))))))))
    (rec (1- (length xs)) 0)))

(defun 5b (data)
   (destructuring-bind (dict updates) data
     (flet ((fixed-mid (xs) (aref-mid (fix-order xs dict)))
            (orderedp (xs) (update-ordered-p xs dict)))
       (apply #'+ (mapcar #'fixed-mid (remove-if #'orderedp updates ))))))
