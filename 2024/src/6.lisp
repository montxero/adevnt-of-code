(in-package :advent)



(defun 6-data (fname &key (type 'character))
  (with-open-file (s (get-data-path fname))
    (let ((eof (gensym)))
      (do ((lst () (push line lst))
           (line (read-line s nil eof nil) (read-line s nil eof nil))
           (rows 0 (1+ rows))
           (cols 0 (max cols (length line))))
          ((eq eof line) (grid<-list rows cols type (nreverse lst)))))))


(defun 6a-turn (heading)
  "Return the hading that results from turining 90 degrees"
  (case heading
    (^ '>)
    (v '<)
    (< '^)
    (> 'v)))

(defun 6a-delta (heading)
  "Return the pair (DR DC) that represents the change in row and colum respectively
when moving one step in the direcion of heading."
  (case heading
    (< (list 0 -1))
    (> (list 0 1))
    (^ (list -1 0))
    (v (list 1 0))))


(defun 6a-move (position heading grid)
  "Return the postion that results from moving 1 step in the current heading if there is no obstacle.
If there is an obstacle i.e. if the next postion is occupied, turn 90 degrees to the right."
  (destructuring-bind (row col) (mapcar #'+ position (6a-delta heading))
    (if (and (array-in-bounds-p grid row col)
             (char= #\# (aref grid row col)))
        (list position (6a-turn heading))
        (list (list row col) heading))))


(defun 6-move (position-heading grid)
  "Return the postion that results from moving 1 step in the current heading if there is no obstacle.
If there is an obstacle i.e. if the next postion is occupied, turn 90 degrees to the right.

Parameters
----------
POSITION-HEADING: ((ROW COL) HEADING)
GRID: 2D ARRAY
"
  (destructuring-bind (p h) position-heading
    (6a-move p h grid)))


(defun 6-pos-in-grid-p (ph grid)
  "Return a true value if the position in PH is within the grid"
  (array-in-bounds-p grid (caar ph) (cadar ph)))


(defun 6a-pos-heading (grid)
  (destructuring-bind (r c) (array-dimensions grid)
    (dotimes (i (* r c))
      (case (row-major-aref grid i)
        (#\^ (return (list (row-col<-row-major-index i r) '^)))
        (#\> (return (list (row-col<-row-major-index i r) '>)))
        (#\v (return (list (row-col<-row-major-index i r) 'v)))
        (#\< (return (list (row-col<-row-major-index i r) '<)))))))


(defun row-col<-row-major-index (index nrows)
  "Return the pair (ROW COL) of the indices that corresponds to the row major index INDEX."
  (multiple-value-bind (r c) (floor index nrows)
    (list r c)))



(defun 6a (fname)
  (let ((grid (6-data (get-data-path fname))))
    (flet ((move (ph)
             (destructuring-bind (p h) ph
               (6a-move p h grid)))
           (pos-in-grid-p (ph)
             (let ((r (caar ph))
                   (c (cadar ph)))
               (array-in-bounds-p grid r c))))
      (do ((visited () (pushnew ph visited :key 'car :test 'equal))
           (ph (6a-pos-heading grid) (move ph)))
          ((not (pos-in-grid-p ph)) (length visited))))))


(defun 6b-run (grid)
  (do ((visited () (push ph visited))
       (ph (6a-pos-heading grid) (6-move ph grid)))
      ((or (member ph visited :test #'equal)
           (not (6-pos-in-grid-p ph grid)))
       (if (6-pos-in-grid-p ph grid) 1 0))))


(defun 6b (fname) ;brute force solution (testing each grid position in sequence.
  (do ((grid (6-data (get-data-path fname)))
       (count 0)
       (i 0 (1+ i)))
      ((= i (array-total-size grid)) count)
    (let ((c (row-major-aref grid i)))
      (unless (find c "#<^>v")
        (setf (row-major-aref grid i) #\#)
        (incf count (6b-run grid))
        (setf (row-major-aref grid i) c)))))
