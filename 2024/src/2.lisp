(in-package :advent)


(defun report-safe-p (report)
  "Return T iff REPORT is safe. A report is safe iff
1. Report is either a strictly increasing or strictly decreasing sequence.
2. Any 2 adjacent numbers in the sequence differ by at least 1 and at most 3.
"
  (let ((xs (mapcar #'- report (cdr report))))
    (cond ((minusp (car xs))
           (every (lambda (x) (<= -3 x -1)) xs))
          ((plusp (car xs))
           (every (lambda (x) (<= 1 x 3)) xs)))))


(defun 2a ()
  (with-open-file (s (get-data-path "2"))
    (let ((eof (gensym)))
     (do ((safe 0)
          (line (read-line s nil eof nil) (read-line s nil eof nil)))
         ((eq line eof) safe)
       (let ((report (mapcar #'parse-integer
                             (xero.utils:partition (lambda (c) (char= #\space c)) line))))
         (when (report-safe-p report)
           (incf safe)))))))


(defun drop-nth (n sequence)
  (concatenate (type-of sequence)
               (copy-seq (subseq sequence 0 n))
               (copy-seq (subseq sequence (1+ n)))))


(defun report-safe-2-p (report)
  "Return T iff REPORT can be mad safe by removing a single number. A report is safe iff
1. Report is either a strictly increasing or strictly decreasing sequence.
2. Any 2 adjacent numbers in the sequence differ by at least 1 and at most 3.
"
  (labels ((rec (xs)
             (when xs
               (destructuring-bind (y . ys) xs
                 (if (report-safe-p y) t (rec ys))))))
    (rec (mapcar (lambda (i) (drop-nth i report)) (xero.utils:iota (length report))))))


(defun 2b ()
  (with-open-file (s (get-data-path "2"))
    (let ((eof (gensym)))
     (do ((safe 0)
          (line (read-line s nil eof nil) (read-line s nil eof nil)))
         ((eq line eof) safe)
       (let ((report (mapcar #'parse-integer
                             (xero.utils:partition (lambda (c) (char= #\space c)) line))))
         (when (report-safe-2-p report)
           (incf safe)))))))
