#||
Utilities for AOC 2024
||#

(defpackage #:advent
  (:use #:cl)
  (:export :map-file))

(in-package #:advent)



(defparameter +data-directory+
  (make-pathname :directory "home/xero/common-lisp/advent/2024/data")
  "Data directory")


(defun map-stream (function stream)
  "Apply FUNCTION to every Lisp expression in STREAM and return NIL."
  (let ((eos (gensym)))
    (do ((x (read stream nil eos nil) (read stream nil eos nil)))
        ((eq x eos) nil)
      (funcall function x))))


(defun map-file (function filespec)
  "Apply FUNCTION to every Lisp expression in the file associated with FILESPEC and return NIL."
  (with-open-file (s filespec)
    (map-stream function s)))


(defun lines<-file (filespec)
  "Return a list of the lines contiained in the file described by FILESPEC"
  (with-open-file (s filespec)
    (let ((eof (gensym)))
      (do ((lines () (push line lines))
           (line (read-line s nil eof nil) (read-line s nil eof nil)))
          ((eq line eof) (nreverse lines))))))


(defun get-data-path (question)
  "Return the PATH object pointing to the file for QUESTION

Parameter
---------
QUESTION: STRING, the question to be solved.

Returns
-------
Path object

Example
-------

(get-data-path \"1a\")
  #P\"/home/xero/common-lisp/advent/data/1a\"
"
  (merge-pathnames +data-directory+ question))
