(in-package :advent)


(defun 9-data (fname)
  (with-open-file (s fname)
    (map 'list #'digit-char-p (read-line s))))


(defun 9a-vector (data)
  (let* ((d (apply #'+ data))
         (xs (make-array d :element-type 'integer :initial-element -1)))
    (loop for n in data
          and spacep = nil then (not spacep)
          and i = 0 then i
          and j = 0 then j
          do (if spacep
                 (incf j n)
                 (progn
                   (loop for k below n
                         do (setf (aref xs (+ k j)) i))
                   (incf i)
                   (incf j n))))
    xs))


(defun 9a (vector)
  (loop for i = 0 then i
        for j = (1- (length vector)) then j
        for x = (aref vector i) then (aref vector i)
        for y = (aref vector j) then (aref vector j)
        until (> i j)
        do (cond ((= -1 y) (decf j))
                 ((> x -1) (incf i))
                 (t (setf (aref vector i) (aref vector j))
                    (setf (aref vector j) -1)))
        finally (return (loop for i upto j
                              sum (* i (aref vector i)) into result
                              finally (return result)))))

(9a (9a-vector (9-data "/home/xero/common-lisp/advent/2024/data/9t")))


(defun 9-next-file-block (vector i)
  (unless (<= i 0)
    (loop for j downfrom i
          for x = (aref vector j) then (aref vector j)
          until (or (zerop j) (/= -1 x))
          finally
             (return
               (loop for k downfrom j
                     until (or (> 0 k) (/= x (aref vector k)))
                     finally (return (list (1+ k) j)))))))


(loop for v = (9a-vector (9-data "/home/xero/common-lisp/advent/2024/data/9")) then v
      for i = (1- (length v)) then (car f)
      for f = (9-next-file-block v i) then (9-next-file-block v (1- i))
      until (<= i 1)
      collect f)

(defun 9-next-free-block (vector i)
  (loop for j upfrom i
        until (or (= j (length vector)) (= -1 (aref vector j)))
        finally
           (return (loop for k upfrom j
                         until (or (= k (length vector)) (/= -1 (aref vector k)))
                         finally (return (list j (1- k)))))))

(loop for v = (9a-vector (9-data "/home/xero/common-lisp/advent/2024/data/9t")) then v
      for i = 0 then (cadr f)
      for f = (9-next-free-block v i) then (9-next-free-block v (1+ i))
      until (>= (cadr f) (1- (length v)))
      collect f)


(defun 9b (vector)
  (flet ((next-file (i) (9-next-file-block vector i))
         (next-space (i) (9-next-free-block vector i)))

    (loop for fi = (next-file (1- (length vector))) then (next-file  (1- (car fi)))
          until (zerop (car fi))
          do (loop for fr = (next-space 0) then (next-space (1+ (cadr fr)))
                   and moved = nil
                   until (or moved (> (car fr) (car fi)))
                   do (when (<= (- (cadr fi) (car fi)) (- (cadr fr) (car fr)))
                        (loop for i from (car fi) upto (cadr fi)
                              and j from (car fr) upto (cadr fr)
                              do (setf (aref vector j) (aref vector i))
                                 (setf (aref vector i) -1)
                              finally (setf moved t))))
          finally (return (loop for i upto (1- (length vector))
                                sum (* i (if (= (aref vector i) -1) 0 (aref vector i))) into result
                                finally (return result))))))


;(9b (9a-vector (9-data "/home/xero/common-lisp/advent/2024/data/9t")))


