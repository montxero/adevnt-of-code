(asdf:defsystem #:aoc-2024
  :author "Xero"
  :description "Xero's advent of code 2024"
  :depends-on ("xero" "cl-ppcre")
  :serial t
  :components ((:module "src"
                :serial t
                :components ((:file "utils")
                             (:file "1")
                             (:file "2")
                             (:file "3")
                             (:file "4")
                             (:file "5")
                             (:file "6")
                             (:file "7")
                             (:file "8")
                             (:file "9")
                             (:file "10")
                             (:file "11")))))
