(in-package :aoc)

(defparameter *6-data* (file-at-once (data-path<-string "6")))

;; 6a
(format t "~&6 a: ~S~%"
        (do ((i 0 (1+ i)))
            ((or (= i (- (length *6-data*) 3))
                 (= 4 (length (remove-duplicates (subseq *6-data* i (+ 4 i))))))
             (+ 4 i))))

;; 6b
(format t "~&6 b: ~S~%"
        (do ((i 0 (1+ i)))
            ((or (= i (- (length *6-data*) 13))
                 (= 14 (length (remove-duplicates (subseq *6-data* i (+ 14 i))))))
             (+ 14 i))))

(format t "~&6 tests: ~S~%"
        (let ((t1 "mjqjpqmgbljsphdztnvjfqwrcgsmlb")
              (t2 "bvwbjplbgvbhsrlpgdmjqwftvncz")
              (t3 "nppdvjthqldpwncqszvftbrmjlhg")
              (t4 "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
              (t5 "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"))
          (flet ((f (s)
                   (do ((i 0 (1+ i)))
                       ((or (= i (- (length s) 13))
                            (= 14 (length (remove-duplicates (subseq s i (+ 14 i))))))
                        (+ 14 i)))))
            (mapcar #'f (list t1 t2 t3 t4 t5)))))
