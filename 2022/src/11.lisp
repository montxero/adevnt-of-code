(in-package :aoc)

(defstruct monkey
  (items nil)
  op
  divisor
  true-dest
  false-dest
  (count 0))

;;; Input was small enough to code driectly.
(defparameter *m0*
  #'(lambda ()
      (make-monkey :items (list 83 88 96 79 86 88 70)
                   :op #'(lambda (n) (* 5 n))
                   :divisor 11
                   :true-dest 2
                   :false-dest 3)))
(defparameter *m1*
  #'(lambda ()
      (make-monkey :items (list 59 63 98 85 68 72)
                   :op #'(lambda (n) (* 11 n))
                   :divisor 5
                   :true-dest 4
                   :false-dest 0)))
(defparameter *m2*
  #'(lambda ()
      (make-monkey :items (list 90 79 97 52 90 94 71 70)
                   :op #'(lambda (n) (+ 2 n))
                   :divisor 19
                   :true-dest 5
                   :false-dest 6)))
(defparameter *m3*
  #'(lambda ()
      (make-monkey :items (list 97 55 62)
                   :op #'(lambda (n) (+ 5 n))
                   :divisor 13
                   :true-dest 2
                   :false-dest 6)))
(defparameter *m4*
  #'(lambda ()
      (make-monkey :items (list 74 54 94 76)
                   :op #'(lambda (n) (* n n))
                   :divisor 7
                   :true-dest 0
                   :false-dest 3)))
(defparameter *m5*
  #'(lambda ()
      (make-monkey :items (list 58)
                   :op #'(lambda (n) (+ 4 n))
                   :divisor 17
                   :true-dest 7
                   :false-dest 1)))
(defparameter *m6*
  #'(lambda ()
      (make-monkey :items (list 66 63)
                   :op #'(lambda (n) (+ 6 n))
                   :divisor 2
                   :true-dest 7
                   :false-dest 5)))
(defparameter *m7*
  #'(lambda ()
      (make-monkey :items (list 56 56 90 96 68)
                   :op #'(lambda (n) (+ 7 n))
                   :divisor 3
                   :true-dest 4
                   :false-dest 1)))

(defparameter *Z* 9699690)

(defun make-monkey-array ()
  (apply #'vector (mapcar #'funcall (list *m0* *m1* *m2* *m3* *m4* *m5* *m6* *m7*))))

(defun simulate-round (monkeys worry-divisor)
  (dovector (m monkeys monkeys)
    (dolist (i (monkey-items m))
      (incf (monkey-count m))
      (let ((j (mod (floor (funcall (monkey-op m) i) worry-divisor) *Z*)))
        (if (zerop (rem j (monkey-divisor m)))
            (push j (monkey-items (svref monkeys (monkey-true-dest m))))
            (push j (monkey-items (svref monkeys (monkey-false-dest m)))))))
    (setf (monkey-items m) nil)))

(defun simulate-n-rounds (n monkeys worry-divisor)
  (dotimes (i n monkeys)
    (simulate-round monkeys worry-divisor)))

(defun monkey-business (monkeys)
  (apply #'* (take 2 (sort (map 'list #'monkey-count monkeys) #'>))))

;; 11a
(format t "~&11 a: ~S~%"
        (monkey-business (simulate-n-rounds 20 (make-monkey-array) 3)))

;; 11b
(format t "~&11 b: ~S~%"
        (monkey-business (simulate-n-rounds 10000 (make-monkey-array) 1)))
