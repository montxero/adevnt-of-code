(in-package :aoc)

(defparameter *9-raw-data* (data-list<-string "9"))
(defparameter *9-moves*
   (mapcan (lambda (p)
             (destructuring-bind (a b) (partition-on-whitespace p)
               (make-list (parse-integer b) :initial-element (char  a 0))))
           *9-RAW-DATA*))

(defparameter *9-test-moves*
 (mapcan (lambda (p)
             (destructuring-bind (a b) (partition-on-whitespace p)
               (make-list (parse-integer b) :initial-element (char  a 0))))
         (data-list<-string "9-test")))

;; data abstraction
(defun make-point (x y)
  (list x y))

(defun point-diff (p1 p2)
  (mapcar #'- p1 p2))

(defun add-points (p1 p2)
  (mapcar #'+ p1 p2))

(let ((table (list (cons #\U (make-point 0 1))
                   (cons #\D (make-point 0 -1))
                   (cons #\L (make-point -1 0))
                   (cons #\R (make-point 1 0)) )))
  (defun move-point (location direction)
    (add-points location (cdr (assoc direction table)))))

(let ((table (mapcar #'(lambda (x)
                         (destructuring-bind (x1 y1 x2 y2) x
                           (list (make-point x1 y1) (make-point x2 y2))))
                     (group (list 2 0 1 0
                                  -2 0 -1 0
                                  0 2 0 1
                                  0 -2 0 -1
                                  1 2 1 1
                                  1 -2 1 -1
                                  -1 2 -1 1
                                  -1 -2 -1 -1
                                  2 1 1 1
                                  2 -1 1 -1
                                  -2 1 -1 1
                                  -2 -1 -1 -1
                                  2 2 1 1
                                  2 -2 1 -1
                                  -2 -2 -1 -1
                                  -2 2 -1 1)
                            4))))
  (defun move-relative (reference knot)
    (let ((diff (point-diff reference knot)))
      (add-points knot
                  (or (cadr (assoc diff table :test #'equal))
                      (make-point 0 0))))))
;; simulation
(defun simulate (moves knots)
  "Return a list of the reverse chornological position history of each knot in order.
Each history is a list of positions."
  (labels ((sim (moves history)
             (if (null moves)
                 history
                 (destructuring-bind (head . ks) (mapcar #'car history)
                   (sim (cdr moves) (mapcar #'cons
                                            (casscade ks (list (move-point head (car moves))))
                                            history)))))
           (casscade (knots acc)
             (if (null knots)
                 (nreverse acc)
                 (casscade (cdr knots)
                           (cons (move-relative (car acc) (car knots))
                                 acc)))))
    (sim moves (mapcar #'list knots))))

(defun simulate-n-knots (n moves)
  (let ((ks nil))
    (dotimes (i n)
      (push (make-point 0 0) ks))
    (length (remove-duplicates (last1 (simulate moves ks)) :test #'equal))))

;; 9a & 9b
(format t "~&9 a: ~S" (simulate-n-knots 2 *9-moves*))
(format t "~&9 b: ~S" (simulate-n-knots 10 *9-moves*))


