(in-package :aoc)

(defparameter *12-data* (data-list<-string "12"))

(defparameter *12-grid*
  (let ((r (length *12-data*))
        (c (length (car *12-data*))))
    (make-array (list r c) :element-type 'character :initial-contents *12-data*)))

;;; data abstraction
;; The maze graph has nodes which are indices and edges which are connections between indices.
;; An index is a pair of y-x coordinates.
(defun make-node (y x) (list y x))
(defun node-x (node) (car node))
(defun node-y (node) (cadr node))
(defun make-edge (src dest) (list src dest))
(defun edge-source (edge) (car edge))
(defun edge-dest (edge) (cadr edge))

;; grid operators
(defun get-adjacent (node grid direction)
  (flet ((new-x (x direction)
           (case direction
             ((:up :down) x)
             (:left (1- x))
             (:right (1+ x))))
         (new-y (y direction)
           (case direction
             ((:left :right) y)
             (:up (1- y))
             (:down (1+ y)))))
    (let ((x (new-x (node-x node) direction))
          (y (new-y (node-y node) direction)))
      (when (array-in-bounds-p grid x y)
        (make-node y x)))))

(defun get-all-adjacent (node grid)
  (seive #'(lambda (d) (get-adjacent node grid d)) (list :up :down :left :right)))

(defun height (char)
  (let ((dict (map 'list #'list *english-lowercase-alphabets* (iota 26))))
    (cond ((char= #\S char) 0) 
          ((char= #\E char) 25)
          (t (cadr (assoc char dict))))))

(defun reachable-p (src-height dest-height)
  (<= (- dest-height src-height) 1))

(defun index (grid node)
  (aref grid (node-y node) (node-x node)))

(defun get-reachable-neighbours (node grid)
  (compose #'height #'(lambda (n) (index grid n))))

