(in-package :aoc)

(defparameter *4-data* (data-list<-string "4"))


(defun parse-line (line)
  (flet ((parse-range (s)
           (mapcar #'parse-integer (partition #'(lambda (c) (char= #\- c)) s))))
    (mapcar #'parse-range (partition #'(lambda (c) (char= #\, c)) line))))


(defparameter *4-ranges* (mapcar #'parse-line *4-data*))


(defun fully-contains-p (range1 range2)
  "return a true value iff range1 fully contains range2"
  (destructuring-bind ((s1 e1) (s2 e2)) (list range1 range2)
    (and (<= s1 s2) (>= e1 e2))))

;; 4a
(format t "~&4 a: ~S~%"
        (count-if #'(lambda (pair)
                      (destructuring-bind (r1 r2) pair
                        (or (fully-contains-p r1 r2) (fully-contains-p r2 r1))))
                  *4-ranges*))


;; 4b
(defun overlaps-p (range1 range2)
  "return a true value iff there is any overlap between range1 and range2"
  (flet ((check (r1 r2)
           (destructuring-bind ((s1 e1) (s2 e2)) (list r1 r2)
             (and (<= s1 s2) (>= e1 s2)))))
    (or (check range1 range2) (check range2 range1))))

(format t "~&4 b: ~S~%"
        (count-if #'(lambda (pair)
                      (destructuring-bind (r1 r2) pair
                        (overlaps-p r1 r2)))
                  *4-ranges*))
