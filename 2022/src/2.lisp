;; depends on xero utils
;; (ql:quickload :xero)
;; (in-package :xero.utils)
#||
(defparameter *problem-directory* "/home/xero/common-lisp/advent/2022/data/")
(defparameter *negative-infinity* sb-ext:double-float-negative-infinity)


(defun data-path<-string (str)
  (concatenate 'string *problem-directory* str))


(defun file-at-once (filespec &rest open-args)
  (with-open-stream (stream (apply #'open filespec
                                     open-args))
    (let* ((buffer
            (make-array (file-length stream)
                        :element-type
                        (stream-element-type stream)
                        :fill-pointer t))
           (position (read-sequence buffer stream)))
      (setf (fill-pointer buffer) position)
      buffer)))

||#

(in-package :aoc)
(defparameter *2-data* (file-at-once (data-path<-string "2")))

(defparameter *r-p-s-wins*  (list (cons 'rock 'scissors)
                                  (cons 'scissors 'paper)
                                  (cons 'paper 'rock))
  "rock-paper-scissors hand wins: rock wins scissors, scissors wins paper and paper wins rock")


;; 2a
(defun single-round (hand1 hand2)
  "Return the winning hand from a single round rock-paper-scissors."
  (unless (eq hand1 hand2)
    (or (and (eq hand2 (cdr (assoc hand1 *r-p-s-wins*))) hand1)
        hand2)))


(defun score-round (hand1 hand2)
  "return hand1's score from a single round of rock-paper-scissors."
  (flet ((play (hand1 hand2)
           (let ((win (single-round hand1 hand2)))
             (cond ((null win) 3)
                   ((eq win hand1) 6)
                   (t 0)))))
    (let ((hand-values (list (cons 'rock 1)
                             (cons 'paper 2)
                             (cons 'scissors 3))))
      (+ (play hand1 hand2) (cdr (assoc hand1 hand-values))))))


(defun hand<-char (chr)
  "return the rock-paper-scissors hand associated with chr"
  (cond ((some #'(lambda (c) (char= c chr)) "AX") 'rock)
        ((some #'(lambda (c) (char= c chr)) "BY") 'paper)
        ((some #'(lambda (c) (char= c chr)) "CZ") 'scissors)))

;; 2a answer
(format t "~&2 a: ~S"
        (apply #'+
               (mapcar #'(lambda (r)
                           (destructuring-bind (hand2 hand1) (seive #'hand<-char r)
                             (score-round hand1 hand2)))
                       (partition #'(lambda (c) (char= #\Newline c)) *2-data*))))

;; 2b
(defun hand<-plan (opponent-hand plan)
  "return the rock-paper-scissors hand to play given a plan and the opponents hand.
X -> loose
Y -> draw
Z -> win."
  (case plan
    (#\X (cdr (assoc opponent-hand *r-p-s-wins*)))
    (#\Y opponent-hand)
    (#\Z (car (rassoc opponent-hand *r-p-s-wins*)))))

(format t "~&2 b. ~S"
        (apply #'+
               (mapcar #'(lambda (r)
                           (let ((opponent-hand (hand<-char (char r 0)))
                                 (plan (char r 2)))
                             (score-round (hand<-plan opponent-hand plan) opponent-hand)))
                       (partition #'(lambda (c) (char= #\Newline c)) *2-data*))))
