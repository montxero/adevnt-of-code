(in-package :aoc)

(defparameter *3-data* (data-list<-string "3"))
(defparameter *item-set* "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")


(defun priority (char)
  (1+  (position char *item-set*)))


;; 3a
(defun common-item-priority (russack)
  (let ((mid (/ (length russack) 2)))
    (priority (car (intersection  (coerce (subseq russack 0 mid) 'list)
                                  (coerce (subseq russack mid) 'list))))))


(format t "~&3 a: ~S"
        (apply #'+ (mapcar #'common-item-priority *3-data*)))


;;3b
(defun badge-priority (group)
  (priority (car (reduce #'intersection (mapcar #'(lambda (r) (coerce r 'list)) group)))))

(format t "~&3 b: ~S"
        (apply #'+ (mapcar #'badge-priority (group *3-data* 3))))
