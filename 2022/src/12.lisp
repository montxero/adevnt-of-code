(in-package :aoc)

(defparameter *12-data* (data-list<-string "12"))

(defparameter *12-grid*
  (let ((r (length *12-data*))
        (c (length (car *12-data*))))
    (make-array (list r c) :element-type 'character :initial-contents *12-data*)))

;; operators
(defun height (char)
  (let ((dict (map 'list #'list *english-lowercase-alphabets* (iota 26))))
    (cond ((char= #\S char) 0) 
          ((char= #\E char) 25)
          (t (cadr (assoc char dict))))))

(defun up (state)
  (let ((x (state-x state))
        (y (state-y state)))
    (when (array-in-bounds-p *12-grid* x (1+ y))
      (make-state-from-indices x (1+ y)))))

(defun down (state)
  (let ((x (state-x state))
        (y (state-y state)))
    (when (array-in-bounds-p *12-grid* x (1- y))
      (make-state-from-indices x (1- y)))))

(defun left (state)
  (let ((x (state-x state))
        (y (state-y state)))
    (when (array-in-bounds-p *12-grid* (1- x) y)
      (make-state-from-indices (1- x) y))))

(defun right (state)
  (let ((x (state-x state))
        (y (state-y state)))
    (when (array-in-bounds-p *12-grid* (1+ x) y)
      (make-state-from-indices (1+ x) y))))

(defun height-okay-p (dest src)
  (>= 1 (- (state-height dest) (state-height src))))

(defun cyclicp (path)
  (not (= (length path) (length (remove-duplicates path :test #'state-equal-p)))))

(defun state-equal-p (s1 s2)
  (and (= (state-x s1) (state-x s2))
       (= (state-y s1) (state-y s2))))

(defun generate-feasible-paths (path)
  (let ((s (car path)))
    (remove-if-not (disjoin #'(lambda (p) (height-okay-p (car p) (cadr p)))
                            #'cyclicp)
                   (mapcar #'(lambda (s) (cons s path))
                           (seive #'(lambda(f) (funcall f s))
                                  (list #'up #'down #'left #'right))))))

;; state representation
(defstruct state
  x y c)

(defun make-state-from-indices (x y)
  (make-state :x x :y y :c (aref *12-grid* x y)))

(defun state-height (s)
  (height (state-c s)))

;; searcher
(defun goal-search (state goal-fn)
  (bfs goal-fn (list (list state))))

(defun bfs (goal-fn queue)
  (if (null queue)
      nil
      (let ((path (car queue)))
        (if (funcall goal-fn (car path))
            path
            (bfs goal-fn
                 (append (cdr queue)
                         (generate-feasible-paths path)))))))

(defvar *start-postion*
  (destructuring-bind (r c) (array-dimensions *12-grid*)
       (do ((i 0 (mod (+ 1 i) r))
            (j 0 (mod (+ 1 j) c))
            (n 0 (1+ n)))
           ((char= #\S (aref *12-grid* i j)) (list i j)))))

(defvar *end-postion*
  (destructuring-bind (r c) (array-dimensions *12-grid*)
       (do ((i 0 (mod (+ 1 i) r))
            (j 0 (mod (+ 1 j) c))
            (n 0 (1+ n)))
           ((char= #\E (aref *12-grid* i j)) (list i j)))))

(defvar *start-state* (apply #'make-state-from-indices *start-postion*))
(defvar *end-state* (apply #'make-state-from-indices *end-postion*))

;; (format t "~&12 a: ~S~%"
;;         (length
;;          (goal-search *start-state*
;;                       #'(lambda (s) (state-equal-p s *end-state*)))))
