(in-package :aoc)

(defparameter *8-data* (data-list<-string "8"))
(defparameter *8-rows* (mapcar #'(lambda (r) (map 'list #'digit-char-p r)) *8-data*))
(defparameter *8-grid* (make-array '(99 99) :initial-contents *8-rows*))

(defun visiblep (row col grid)
  "return t iff the tree at row and col is visible."
  (destructuring-bind (r c) (array-dimensions grid)
    (or (visible-in-row-p row col grid)
        (visible-in-col-p row col grid))))

(defun visible-in-row-p (row col grid)
  "return t iff the tree at row, col is visible on the row."
  (let ((lr (partition-at-index col (get-row row grid)))
        (h (aref grid row col)))
    (destructuring-bind (left right) lr
      (or (every #'(lambda (x) (> h x)) left)
          (every #'(lambda (x) (> h x)) right)))))

(defun visible-in-col-p (row col grid)
  "return t iff the tree at row, col is visible on the column."
  (let ((tb  (partition-at-index row (get-column col grid)))
        (h (aref grid row col)))
    (destructuring-bind (top bottom) tb
      (or (every #'(lambda (x) (> h x)) top)
          (every #'(lambda (x) (> h x)) bottom)))))

(defun get-row (row matrix)
  "return row n as a vector"
  (destructuring-bind (_ c) (array-dimensions matrix)
    (declare (ignore _))
    (let ((result nil))
      (dotimes (i c (nreverse result))
        (push (aref matrix row i) result)))))

(defun get-column (col matrix)
  "return column n as a vector"
  (destructuring-bind (r _) (array-dimensions matrix)
    (declare (ignore _))
    (let ((result nil))
      (dotimes (i r (nreverse result))
        (push (aref matrix i col) result)))))

(defun partition-at-index (n seq)
  "return a list of 2 sequences s1 and s2 where s1 is a sequence
containing the initial subsequence of seq before index n and s2 contains elements after index n.
Both subsequences are in order of apperance in seq."
  (list (take n seq) (drop (1+ n) seq)))

;; 8 a
(format t "~&8 a: ~S~%"
        (let ((visibles nil))
          (destructuring-bind (r c) (array-dimensions *8-grid*)
            (dotimes (i r (length visibles))
              (dotimes (j c)
                (and (visiblep i j *8-grid*) (push (aref *8-grid* i j) visibles)))))))

;; 8 b
(defun scenic-score (row col grid)
  "return the scenic score of the tree at row, col in grid."
    (let ((height (aref grid row col))
          (lr (partition-at-index col (get-row row grid)))
          (ud (partition-at-index row (get-column col grid))))
      (flet ((view (xs)
               (let ((l (length (take-while #'(lambda (h) (> height h)) xs))))
                 (if (= l (length xs))
                     l
                     (1+ l)))))
        (let ((lc (view (reverse (car lr))))
              (rc (view (cadr lr)))
              (uc (view (reverse (car ud))))
              (dc (view (cadr ud))))
          (* lc rc uc dc)))))

(format t "~&8 b: ~S~%"
        (let ((best *negative-infinity*))
          (destructuring-bind (r c) (array-dimensions *8-grid*)
            (dotimes (i r best)
              (dotimes (j c)
                (setf best (max best (scenic-score i j *8-grid*))))))))
