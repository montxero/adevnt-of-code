(in-package :aoc)

(defparameter *1-data* (data-list<-string "1"))
(defparameter *calorie-bags* (partition (lambda (w) (string= w "")) *1-data*))

;; 1a
(format t "~&1 a: ~S"
        (apply #'max
               (mapcar (lambda (x) (apply #'+ (mapcar (lambda (w) (parse-integer w)) x)))
                       *calorie-bags*)))


;; 1b
(defstruct pipe3
  (a *negative-infinity*) (b *negative-infinity*) (c *negative-infinity*))


(defun insert-into-pipe3 (n pipe3)
  (cond ((> n (pipe3-a pipe3))
         (psetf (pipe3-c pipe3) (pipe3-b pipe3)
                (pipe3-b pipe3) (pipe3-a pipe3)
                (pipe3-a pipe3) n))
        ((> n (pipe3-b pipe3))
         (psetf (pipe3-c pipe3) (pipe3-b pipe3)
                (pipe3-b pipe3) n))
        ((> n (pipe3-c pipe3))
         (setf (pipe3-c pipe3) n))))


(defun sum-pipe3 (p)
  (+ (pipe3-a p) (pipe3-b p) (pipe3-c p)))

;; great. works in linear time on the grouped list
(format t "~&1 b O(n): ~S"
        (let ((p (make-pipe3)))
          (dosequence (x *calorie-bags* (sum-pipe3 p))
            (insert-into-pipe3 (apply #'+ (mapcar (lambda (s) (parse-integer s)) x)) p))))


;; alternative O(nlog n)
(format t "~&1 b O(n log n): ~S"
        (apply #'+
               (take 3
                     (sort (mapcar (lambda (x) (apply #'+ (mapcar (lambda (w) (parse-integer w)) x)))
                                   *calorie-bags*)
                           #'>))))
