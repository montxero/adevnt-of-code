(in-package :aoc)

(defparameter *problem-directory* "/home/xero/common-lisp/advent/2022/data/")
(defparameter *negative-infinity* sb-ext:double-float-negative-infinity)
(defparameter *english-uppercase-alphabets* "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
(defparameter *english-lowercase-alphabets* "abcdefghijklmnopqrstuvwxyz")
(defparameter *english-alphabets*
  (concatenate 'string
               *english-lowercase-alphabets*
               *english-uppercase-alphabets*))

(defun data-path<-string (str)
  (concatenate 'string *problem-directory* str))


(defun file-at-once (filespec &rest open-args)
  (with-open-stream (stream (apply #'open filespec
                                     open-args))
    (let* ((buffer
            (make-array (file-length stream)
                        :element-type
                        (stream-element-type stream)
                        :fill-pointer t))
           (position (read-sequence buffer stream)))
      (setf (fill-pointer buffer) position)
      buffer)))

(defun data-list<-string (file-name)
  "return the data contained in file-name"
  (partition #'(lambda (c) (char= #\newline c)) (file-at-once (data-path<-string file-name))))

(defun prefixp (seq1 seq2)
  "Return t iff s is a prefex of seq2. i.e seq starts with seq1"
  (equal seq1 (take (length seq1) seq2)))

(defun suffixp (seq1 seq2)
  "Return t iff seq2 ends with seq1"
  (equal seq1 (reverse (take (length seq1) (reverse seq2)))))

(defun partition-on-whitespace (seq)
  "Partition seq on whitespace"
  (partition #'(lambda (c) (member c (list #\space #\tab))) seq))

(defun enumerate (sequence)
  (let ((result nil)
        (i 0))
    (dosequence (x sequence (nreverse result))
      (push (list i x) result)
      (incf i))))

(defun iota (n) (loop for i from 0 upto n collect i))
