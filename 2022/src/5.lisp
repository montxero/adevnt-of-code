(in-package :aoc)

(defparameter *5-raw-data* (data-list<-string "5"))

(partition #'(lambda (c) (string= c "")) *5-RAW-DATA*)


(defun parse-move (string)
  (destructuring-bind (a n f source k dest) (partition #'(lambda (c) (char= #\space c)) string)
    (declare (ignore a f k))
    (mapcar #'parse-integer (list n source dest))))


(destructuring-bind (stacks* moves) (partition #'(lambda (c) (string= "" c)) *5-RAW-DATA*)
  (let ((rows (mapcar #'(lambda (r) (group r 4)) (butlast stacks*))))
    (defparameter *initial-stack*
      (mapcar (lambda (r) (mapcar (lambda (x) (car (seive (lambda (c) (and (alpha-char-p c) c)) x))) r)) rows))
    (defparameter *moves* (mapcar #'parse-move moves))))

;;5a
(format t "~&5 a: ~S~%"
        (let ((stack (list (cons 1 (remove nil (mapcar #'first *initial-stack*)))
                           (cons 2 (remove nil (mapcar #'second *initial-stack*)))
                           (cons 3 (remove nil (mapcar #'third *initial-stack*)))
                           (cons 4 (remove nil (mapcar #'fourth *initial-stack*)))
                           (cons 5 (remove nil (mapcar #'fifth *initial-stack*)))
                           (cons 6 (remove nil (mapcar #'sixth *initial-stack*)))
                           (cons 7 (remove nil (mapcar #'seventh *initial-stack*)))
                           (cons 8 (remove nil (mapcar #'eighth *initial-stack*)))
                           (cons 9 (remove nil (mapcar #'ninth *initial-stack*))))))
          (labels ((move (lst)
                     (destructuring-bind (n source dest) lst
                       (let ((crates (nreverse (take n (cdr (assoc source stack))))))
                         (setf (cdr (assoc dest stack)) (nconc crates (cdr (assoc dest stack))))
                         (setf (cdr (assoc source stack))  (drop n (cdr (assoc source stack))))))))
            (mapcar #'move *moves*)
            (coerce (mapcar #'cadr stack) 'string))))


;; 5b
(format t "~&5 a: ~S~%"
        (let ((stack (list (cons 1 (remove nil (mapcar #'first *initial-stack*)))
                           (cons 2 (remove nil (mapcar #'second *initial-stack*)))
                           (cons 3 (remove nil (mapcar #'third *initial-stack*)))
                           (cons 4 (remove nil (mapcar #'fourth *initial-stack*)))
                           (cons 5 (remove nil (mapcar #'fifth *initial-stack*)))
                           (cons 6 (remove nil (mapcar #'sixth *initial-stack*)))
                           (cons 7 (remove nil (mapcar #'seventh *initial-stack*)))
                           (cons 8 (remove nil (mapcar #'eighth *initial-stack*)))
                           (cons 9 (remove nil (mapcar #'ninth *initial-stack*))))))
          (labels ((move (lst)
                     (destructuring-bind (n source dest) lst
                       (let ((crates (take n (cdr (assoc source stack)))))
                         (setf (cdr (assoc dest stack)) (nconc crates (cdr (assoc dest stack))))
                         (setf (cdr (assoc source stack))  (drop n (cdr (assoc source stack))))))))
            (mapcar #'move *moves*)
            (coerce (mapcar #'cadr stack) 'string))))
