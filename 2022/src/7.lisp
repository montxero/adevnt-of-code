(in-package :aoc)

(defparameter *7-data* (data-list<-string "7"))

(defstruct dir
  "Directory structure"
  name (parent nil) (children nil))

(defstruct file
  "File structure"
  name size)

(defun dir-dir-children (dir)
  "return a list of dir's children which are themselves dirs"
  (remove-if-not #'dir-p (dir-children dir)))

(defun dir-file-children (dir)
  "return a list of dir's children which are files."
  (remove-if-not #'file-p (dir-children dir)))

(defun dir-child-p (dir1 dir2)
  "return t iff dir1 is a child of dir2"
  (member (dir-name dir1) (mapcar #'dir-name (dir-children dir2))))

(defun commandp (string)
  "return a true value iff string is a command"
  (prefixp "$" string))

(defun cd-command-p (string)
  (string= "cd" (subseq string 2 4)))

(defun ls-command-p (string)
  (string= "ls" (subseq string 2 4)))

(defun simulate (seq current-node root)
  (if (null seq)
      root
      (destructuring-bind (c . rest) seq
        (cond ((cd-command-p c)
               (let ((dest (subseq c 5)))
                 (cond ((string= "/" dest)
                        (simulate rest root root))
                       ((string= ".." dest)
                        (simulate rest (dir-parent current-node) root))
                       (t
                        (let ((d (car (seive #'(lambda (d)
                                                 (and (dir-p d)
                                                      (string= dest (dir-name d))
                                                      d))
                                             (dir-children current-node)))))
                          (simulate rest d root))))))
              ((ls-command-p c)
               (dosequence (c (take-while (complement #'commandp) rest))
                 (destructuring-bind (s name) (partition-on-whitespace c)
                   (if (string= "dir" s)
                       (unless (member name
                                       (mapcar #'dir-name
                                               (dir-dir-children current-node)))
                         (let ((new-dir (make-dir :name name :parent current-node)))
                           (setf (dir-children current-node)
                                 (push new-dir (dir-children current-node)))))
                       (unless (member name
                                       (mapcar #'file-name
                                               (dir-file-children current-node)))
                         (setf (dir-children current-node)
                               (push (make-file :name name :size (parse-integer s))
                                     (dir-children current-node)))))))
               (simulate (drop-while (complement #'commandp) rest)
                         current-node
                         root))))))

(let ((root (make-dir :name "/")))
  (defparameter *7-tree* (simulate *7-data* root root)))

(defun dir-size (dir)
  (let ((dirs (remove-if-not #'dir-p (dir-children dir)))
        (files (remove-if-not #'file-p (dir-children dir))))
    (+ (apply #'+ (mapcar #'file-size files))
       (apply #'+ (mapcar #'dir-size dirs)))))


(defun get-dirs (dir)
  (remove-if-not #'dir-p (dir-children dir)))

(defun flatten-dir (dir)
  (labels ((rec (queue acc)
             (if (null queue)
                 acc
                 (destructuring-bind (d . q) queue
                   (let ((dirs (get-dirs d)))
                     (dosequence (x dirs)
                       (pushnew x acc))
                     (rec (append q dirs) acc))))))
    (rec (list dir) (list dir))))

;; 7a
(format t "~&7 a: ~S~%"
        (apply #'+
               (mapcar #'car
                       (remove-if #'(lambda (p) (> (car p) 100000))
                                  (mapcar #'(lambda (d) (list (dir-size d) (dir-name d)))
                                          (flatten-dir *7-tree*))))))

;; 7b
(format t "~&7 b: ~S~%"
        (let ((required-space (- 30000000 (- 70000000 (dir-size *7-tree*)))))
          (car (sort (remove-if #'(lambda (p) (< (car p) required-space))
                                 (mapcar #'(lambda (d) (list (dir-size d) (dir-name d)))
                                         (flatten-dir *7-tree*)))
                      #'<
                      :key #'car))))
