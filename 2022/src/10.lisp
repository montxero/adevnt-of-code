(in-package :aoc)

(defparameter *10-data* (data-list<-string "10"))
(defparameter *10a-test-data* (data-list<-string "10a-test"))

(defun simulate (commands outputs)
  (let ((r (car outputs))
        (cmd (car commands))
        (rest (cdr commands)))
    (cond ((null cmd) (nreverse outputs))
          ((string= "noop" cmd)
           (simulate rest (cons r outputs)))
          (t (let ((n (parse-integer (cadr (partition-on-whitespace cmd)))))
               (simulate (cdr commands)
                         (append (list (+ n r) r) outputs)))))))

(defparameter *10-register-history* (simulate *10-data* (list 1)))

;; 10 a
(format t "~&10a : ~S~%"
        (apply #'+
               (mapcar #'(lambda (n) (* n (nth (- n 1) *10-register-history*)))
                       (list 20 60 100 140 180 220))))
;; 10b
(defun visiblep (pixel register)
  (or (= pixel register)
      (= pixel (- register 1))
      (= pixel (+ register 1))))

(format t "~&10 b: ~S~%"
        (let ((register-grid (group *10-register-history* 40)))
          (mapcar #'(lambda (r)
                      (let ((ps (remove-if (complement #'(lambda (x) (apply #'visiblep x))) (enumerate r)))
                            (s (make-string 40 :initial-element #\.)))
                        (dosequence (p ps s)
                          (setf (char s (car p)) #\#))))
                  register-grid)))
