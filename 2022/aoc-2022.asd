(asdf:defsystem #:aoc-2022
  :author "Sey MontXero"
  :description "Xero's advent of code 2022 system"
  :depends-on ("xero")
  :serial t
  :components ((:module "src"
                :serial t
                :components ((:file "package")
                             (:file "utils")
                             (:file "1")
                             (:file "2")
                             (:file "3")
                             (:file "4")
                             (:file "5")
                             (:file "6")
                             (:file "7")
                             (:file "8")
                             (:file "9")
                             (:file "10")
                             (:file "11")
                             (:file "12")))))
